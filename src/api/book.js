import axios from '@/axios'

export function getBookListByType(state) {
  return axios({
    method: 'get',
    url: `/public/book/no/getList/${state.bookPageForm.current}/${state.bookPageForm.size}/${state.searchBookForm.orderType}`,
    params: {
      from: state.searchBookForm.from,
      to: state.searchBookForm.to
    }
  })
}

export function search(state) {
  return axios({
    method: 'get',
    url: `/public/book/no/search/${state.bookPageForm.current}/${state.bookPageForm.size}/${state.searchBookForm.type}/${state.searchBookForm.orderType}`,
    params: {
      str: state.searchBookForm.str,
      from: state.searchBookForm.from,
      to: state.searchBookForm.to
    }
  })
}

export function getBooksByCid(state) {
  return axios({
    method: 'get',
    url: `/public/book/no/getBooksByCid/${state.bookPageForm.current}/${state.bookPageForm.size}/${state.searchBookForm.cid}/${state.searchBookForm.orderType}`,
    params: {
      from: state.searchBookForm.from,
      to: state.searchBookForm.to
    }
  })
}

export function getBooksById(id) {
  return axios({
    method: 'get',
    url: `/public/book/getBookById/${id}`
  })
}
