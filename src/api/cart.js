import axios from '@/axios'

export function delCartByIds(obj) {
  return axios({
    method: 'delete',
    url: '/api/cart/deleteCartByIds',
    data: obj
  })
}

export function addCart(obj) {
  return axios({
    method: 'post',
    url: '/api/cart/addCart',
    data: obj
  })
}

// 获取用户购物车
export function getCartList(uid) {
  return axios({
    method: 'post',
    url: '/api/cart/getCartListByUserId',
    data: {
      userId: uid
    }
  })
}
