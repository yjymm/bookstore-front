import axios from '@/axios'

export function getSecondKillBooks() {
  return axios({
    method: 'get',
    url: `/public/book`
  })
}

export function postSecondKillBook(data) {
  return axios({
    method: 'post',
    url: `/user/secondKill`,
    data: data
  })
}
