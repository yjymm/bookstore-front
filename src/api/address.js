import axios from '@/axios'

export function delAddress(id) {
  return axios({
    method: 'delete',
    url: '/user/address',
    params: {
      id
    }
  })
}

export function setDefaultAdd(payload) {
  return axios({
    method: 'put',
    url: '/user',
    data: {
      id: payload.uid,
      fkAddressId: payload.aid
    }
  })
}

// 添加用户收货地址
export function addUserAddress(addressQo) {
  return axios({
    method: 'post',
    url: '/user/address',
    data: addressQo
  })
}

export function getAddress(uid) {
  return axios({
    method: 'get',
    url: '/user/address',
    params: {
      userId: uid
    }
  })
}
