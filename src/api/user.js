// import Cookies from 'js-cookie'
import axios from '@/axios'

// 放在url中
export function login(form) {
  // return axios.post('/auth/user/login')
  return axios({
    method: 'post',
    url: '/auth/user/login',
    params: form
    // headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
  })
}

// 放在body
export function register(form) {
  return axios({
    method: 'post',
    url: '/user/register',
    data: form
  })
}

export function getInfo() {
  return axios.get('/user/getInfo')
}

export function update(userQo) {
  return axios({
    method: 'put',
    url: '/user',
    data: userQo
  })
}
