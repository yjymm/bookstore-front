import axios from '@/axios'

// 获取订单信息，指定超时时间，默认5秒
export function getOrderById(id) {
  const out = 5000
  return axios({
    method: 'get',
    url: `/order/orderDetail/${id}`,
    timeout: out
  })
}

// 生成订单
export function addOrders(payload) {
  return axios({
    method: 'post',
    url: `/order/save/${payload.uid}`,
    data: payload.orderArr
  })
}

// 获取用户订单列表
export function getOrderList(uid) {
  return axios.get(`/order/getOrderList/${uid}`)
}

// 订单支付
export function payOrders(orders) {
  return axios({
    method: 'put',
    url: '/order/update/status',
    data: orders
  })
}

// 修改订单状态
export function updateOrderStatus(orders) {
  return axios({
    method: 'put',
    url: '/order/update/status',
    data: orders
  })
}

export function delOrder(obj) {
  return axios({
    method: 'delete',
    url: `/order/delete/${obj.uid}/${obj.oid}`
  })
}

// 更新订单地址
export function updateOrder(oid, orderQo) {
  return axios({
    method: 'put',
    url: `/order/update/${oid}`,
    data: orderQo
  })
}

