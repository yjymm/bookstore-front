import axios from '@/axios'

/**
 * 返回书籍的所有分类，并且没有category对象带几本书
 */
export function getList() {
  return axios({
    method: 'get',
    url: '/public/category/getList/1/99/0'
  })
}
