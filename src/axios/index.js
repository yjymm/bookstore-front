import axios from 'axios'
import store from '@/store'
import { Message } from 'element-ui'
import router from '@/router'
import * as common from '@/common/common'

// axios.defaults.baseURL = 'https://www.fastmock.site/mock/185f903dd0738d3330e7bacdf7fa83f8/bs'
axios.defaults.baseURL = 'http://115.29.243.4:8888/'
// axios.defaults.baseURL = 'http://localhost/'
// axios.defaults.baseURL = '/api'

// 添加请求拦截器
axios.interceptors.request.use((config) => {
  // 在发送请求之前先查看当前是否有userToken，有则加上
  const token = common.getToken()
  if (token) {
    // console.log(router)
    // console.log(config)
    if (router.currentRoute.fullPath !== '/lr/login' && router.currentRoute.fullPath !== '/lr/register') {
      config.headers['login_token'] = token
    }
    // if (config.url !== '/auth/user/login' || )
  }
  return config
}, (error) => {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
axios.interceptors.response.use((response) => {
  // 对响应数据做点什么
  const res = response.data

  if (res.code !== 200) {
    Message({
      message: res.message || 'Error',
      type: 'error',
      duration: 4 * 1000
    })
    if (res.code === 444) {
      // token过期了，重新登陆
      store.commit('user/SET_ISLOGIN', false)
      localStorage.removeItem('token')
      common.clear()
      router.push('/lr/login')
    }
    return Promise.reject(new Error(res.message || 'Error'))
  }
  return res
}, (error) => {
  // 对响应错误做点什么
  return Promise.reject(error)
})

export default axios
