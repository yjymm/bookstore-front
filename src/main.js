import Vue from 'vue'
import App from './App.vue'
import router from './router'
// import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex'

import store from './store'
import axios from '@/axios'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI)
Vue.use(Vuex)

Vue.use(VueAxios, axios)
Vue.config.productionTip = false

// eslint-disable-next-line space-before-function-paren
Vue.filter('num2Point', function (val) {
  return val.toFixed(2)
})

new Vue({

  router,
  store,
  render: h => h(App)
}).$mount('#app')
