const getters = {

  // 按照书籍名称或作者搜索的书籍列表
  showBooks: state => state.book.showBooks,
  staticbook: state => state.book.staticbook,
  bookDetail: state => state.book.bookDetail,
  searchBookForm: state => state.book.searchBookForm,
  bookPageForm: state => state.book.bookPageForm,
  activeIndex: state => state.book.activeIndex,
  // 书籍分类
  categoryList: state => state.category.categoryList,

  // user
  isLogin: state => state.user.isLogin,
  userDetail: state => state.user.userDetail,
  userAvatar: state => state.user.userDetail.avatar,
  token: state => state.user.token,
  // address
  userAddressList: state => state.address.userAddressList,

  // cart
  userCart: state => state.cart.userCart,
  buyUserCart: state => state.user.buyUserCart,

  // order
  userOrderList: state => state.order.userOrderList,
  showOrderList: state => state.order.showOrderList,
  orderDetail: state => state.order.orderDetail

}
export default getters
