import { getList } from '@/api/category'
const state = {
  categoryList: ''
  // bookListLink: []
}

const mutations = {
  SET_CATEGORYLIST: (state, categoryList) => {
    state.categoryList = categoryList
  }

}

const actions = {
  getList({ commit }) {
    return new Promise((resolve, reject) => {
      getList().then(res => {
        commit('SET_CATEGORYLIST', res.object.records)
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
