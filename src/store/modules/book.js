import * as book from '@/api/book'
const state = {
  showBooks: [],
  staticbook: [
    {
      id: 1,
      url: require('../../assets/staticbook/static-book-1.jpg')
    }, {
      id: 2,
      url: require('../../assets/staticbook/static-book-2.jpg')
    }, {
      id: 3,
      url: require('../../assets/staticbook/static-book-3.jpg')
    }, {
      id: 4,
      url: require('../../assets/staticbook/static-book-4.jpg')
    }, {
      id: 5,
      url: require('../../assets/staticbook/static-book-5.jpg')
    }

  ],
  // bookDetail的具体图书详情
  bookDetail: '',

  // 分页逻辑参数
  bookPageForm: {
    total: 0,
    size: 18,
    current: 1,
    pages: 0

  },
  // 1:综合排序 2:销量排序，3：从低到高，4：从高到低
  activeIndex: '2',
  // 搜索书籍参数
  searchBookForm: {
    // 1: 书名 2：作者 3：分类
    type: 1,
    // 模糊查询的字符串
    str: '',
    // 是否根据cid查询图书
    getBooksByCid: false,
    cid: '',
    // 图书查询过滤： 1综合排序 2销量降序 3价格从低到高 4价格从高到低
    orderType: 1,
    from: '',
    to: ''
  }

}

const mutations = {
  SET_SHOWBOOKS: (state, bookList) => {
    state.showBooks = bookList
  },
  SET_BOOKDETAIL: (state, bookDetail) => {
    state.bookDetail = bookDetail
  },
  SET_BOOKPAGEFORM: (state, bookPageForm) => {
    state.bookPageForm.current = bookPageForm.current
    state.bookPageForm.size = bookPageForm.size
    state.bookPageForm.total = bookPageForm.total
    state.bookPageForm.pages = bookPageForm.pages
  },
  SET_SEARCHBOOKFORM: (state, searchBookForm) => {
    state.searchBookForm.type = searchBookForm.type
    state.searchBookForm.str = searchBookForm.str
    state.searchBookForm.getBooksByCid = searchBookForm.getBooksByCid
    state.searchBookForm.cid = searchBookForm.cid
    state.searchBookForm.orderType = searchBookForm.orderType
    state.searchBookForm.from = searchBookForm.from
    state.searchBookForm.to = searchBookForm.to
  },
  SET_ORDERTYPE_FROM_TO: (state, obj) => {
    state.searchBookForm.orderType = obj.orderType
    state.searchBookForm.from = obj.from
    state.searchBookForm.to = obj.to
  },
  SET_ACTIVEINDEX: (state, str) => {
    state.activeIndex = str.toString()
  },
  SET_FROM: (state, from) => {
    state.searchBookForm.from = from
  },
  SET_TO: (state, to) => {
    state.searchBookForm.to = to
  }

}

const actions = {
  getList({ commit, state }) {
    return new Promise((resolve, reject) => {
      book.getBookListByType(state).then(res => {
        commit('SET_SHOWBOOKS', res.object.records)
        const bookPageForm = {
          total: res.object.total,
          size: res.object.size,
          current: res.object.current,
          pages: res.object.pages
        }
        commit('SET_BOOKPAGEFORM', bookPageForm)
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  },
  searchBook({ commit, state }) {
    return new Promise((resolve, reject) => {
      book.search(state).then(res => {
        commit('SET_SHOWBOOKS', res.object.records)
        const bookPageForm = {
          total: res.object.total,
          size: res.object.size,
          current: res.object.current,
          pages: res.object.pages
        }
        commit('SET_BOOKPAGEFORM', bookPageForm)
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  },

  getBookById({ commit }, bid) {
    return new Promise((resolve, reject) => {
      book.getBooksById(bid).then(res => {
        commit('SET_BOOKDETAIL', res.object)
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  },

  getBooksByCid({ commit, state }) {
    return new Promise((resolve, reject) => {
      book.getBooksByCid(state).then(res => {
        commit('SET_SHOWBOOKS', res.object.records)
        const bookPageForm = {
          total: res.object.total,
          size: res.object.size,
          current: res.object.current,
          pages: res.object.pages
        }
        commit('SET_BOOKPAGEFORM', bookPageForm)
        resolve(res)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
