
import store from '@/store'
// import * as user from '@/api/user'

// // 恢复图书相关的全局变量
export function initBookPageFormAndSearchBookForm() {
  // 所有回复原始数据
  // store.commit('book/SET_SEARCHSTR', '')
  const bookPageForm = {
    current: 1,
    size: 18,
    total: 0,
    pages: 0
  }
  store.commit('book/SET_BOOKPAGEFORM', bookPageForm)
  const searchBookForm = {
    type: 1,
    str: '',
    getBooksByCid: false,
    cid: '',
    orderType: 1,
    from: '',
    to: ''
  }
  store.commit('book/SET_SEARCHBOOKFORM', searchBookForm)
  store.commit('book/SET_ACTIVEINDEX', '1')
}

// // store用户数据初始化
export function userMessageInit() {
  // user模块数据初始化
  store.commit('user/SET_TOKEN', '')
  store.commit('user/SET_ISLOGIN', false)
  store.commit('user/SET_NAME', '')
  store.commit('user/SET_USERDETAIL', '')
  localStorage.removeItem('token')
  clear()
  initBookPageFormAndSearchBookForm()
}

// // 获取用户所有数据保存在store
export function getUserAllInfo() {
  store.dispatch('user/getUserInfo').then(res => {
    // 本地保存用户info
    localStorage.setItem('userInfo', res.object)
    // 请求用户收货地址
    // console.log('执行了')
    store.dispatch('address/getUserAddress', res.object.id)
    // 请求用户购物车数据
    store.dispatch('cart/getCarts', res.object.id)
    // 请求用户订单数据
    store.dispatch('order/getUserOrderList', res.object.id)
  }).catch(error => {
    console.log(error)
  })
}

// // 获取用户info和addressList
export function getUserInfoAndAddress() {
  return new Promise((resolve, reject) => {
    store.dispatch('user/getUserInfo').then(res => {
      // 本地保存用户info
      localStorage.setItem('userInfo', res.object)
      store.dispatch('address/getUserAddress', res.object.id).then(res => {
        resolve()
      })
    }).catch(() => {
      reject()
    })
  })
}

/**
 *
 * @param {*} key
 *
 * 'token':用户登陆token
 * 'userInfo':用户基本信息
 * 'addressList':用户收货地址列表
 * 'buyUserCart': 用户需要购买的商品列表
 * 'categoryList': 图书类别列表
 * 'userCart':用户购物车
 * 'orderList':用户订单列表
 */
export function getItemFromsStorage(key) {
  return JSON.parse(localStorage.getItem(key))
}
export function setItemToStorage(key, item) {
  localStorage.setItem(key, JSON.stringify(item))
}
export function isExist(key) {
  const obj = localStorage.getItem(key)
  if (obj) {
    return true
  } else {
    return false
  }
}
export function getToken() {
  return JSON.parse(localStorage.getItem('token'))
}

// 清除用户数据，保留token，用来下次自动登陆
export function clear() {
  localStorage.removeItem('userInfo')
  localStorage.removeItem('addressList')
  localStorage.removeItem('buyUserCart')
  localStorage.removeItem('categoryList')
}
